
var assert = require('chai').assert;
var app = require('../public/js/main');



// Matches Won per Year Dummy Data

let matchesDummyData = [
    {
        season: 2016,
    },
    {
        season: 2016,
    },
    {
        season: 2013,
    },
    {
        season: 2014
    }
];

// Matches Won Per Year Testing

describe('Matches Per Year', function () {
    it('Matches Yearly Should Return Number Of Matches Per Year', function () {
        assert.deepEqual(app.matchesYearly(matchesDummyData), { '2013': 1, '2014': 1, '2016': 2 });
    });

    it('Matches Yearly Should Return Object', function () {
        assert.isObject(app.matchesYearly(matchesDummyData));
    })
});



// Matches Won By Each Team Per Year Dummy Data

var matchesWonDummyData = [
    {
        'id': 2,
        'season': 2016,
        'winner': "Mumbai Indians",
    },
    {
        'id': 6,
        'season': 2016,
        'winner': "Kolkata Knight Riders",
    },
    {
        'id': 3,
        'season': 2016,
        'winner': "Rajasthan Royals",
    },
    {
        'id': 5,
        'season': 2016,
        'winner': "Delhi Daredevils",
    },
    {
        'id': 8,
        'season': 2016,
        'winner': "Mumbai Indians",
    },
    {
        'id': 10,
        'season': 2016,
        'winner': "Royal Challengers Bangalore",
    },
    {
        'id': 18,
        'season': 2016,
        'winner': "Mumbai Indians",
    },
    {
        'id': 20,
        'season': 2016,
        'winner': "Kings XI Punjab",
    },

]

// Matches Won Per Year Testing

describe('Matches Won Per Year By Each Team', function () {

    let actual = app.matchesWonPerYear(matchesWonDummyData);
    let expected = {
        'Mumbai Indians': { '2016': 3 },
        'Kolkata Knight Riders': { '2016': 1 },
        'Rajasthan Royals': { '2016': 1 },
        'Delhi Daredevils': { '2016': 1 },
        'Royal Challengers Bangalore': { '2016': 1 },
        'Kings XI Punjab': { '2016': 1 }
    };
    it('Matches Won Per Year By Each Team Should Return Number Of Matches Won BY Each Team Per Year', function () {

        assert.deepEqual(actual, expected);
    });


    it('Matches Won Per Year By Each Team Should Return Object', function () {
        assert.isObject(actual);
    })
});


//   Extra runs conceded per teams for year 2016

let extraRunsDummyData = [
    {
        match_id: 573,
        extra_runs: 2,
        bowling_team: "Royal Challengers Bangalore",
    },
    {
        match_id: 574,
        extra_runs: 4,
        bowling_team: "Royal Challengers Bangalore",
    },
    {
        match_id: 575,
        extra_runs: 0,
        bowling_team: "Mumbai Indians",
    }
];

let valid_match_id_for_extra_runs = [573, 574, 575];


//  Extra run per year testing

describe('Ectra Runs By Each Team In year 2016', function () {
    let expected = { 'Royal Challengers Bangalore': 6, 'Mumbai Indians': 0 };
    let actual = app.extraRuns(extraRunsDummyData, valid_match_id_for_extra_runs);
    it('Extra Runs Should Return Number Of Extra Runs Given By Each', function () {
        assert.deepEqual(actual, expected);
    })


    it('Extra Runs By Each Team Should Return Object', function () {
        assert.isObject(actual);
    })
});



//Economy Dummy Data

let economyDummyData = [
    {
        "match_id": 578,
        "bowler": "TS Mills",
        "extra_runs": 0,
        "total_runs": 0,
    },

    {
        "match_id": 579,
        "bowler": "TS Mills",
        "extra_runs": 0,
        "total_runs": 1,
    },

    {
        "match_id": 580,
        "bowler": "TS Mills",
        "extra_runs": 0,
        "total_runs": 4,
    },

    {
        "match_id": 581,
        "bowler": "TS Mills",
        "extra_runs": 4,
        "total_runs": 4,
    },


];

let valid_match_id_for_economy = [578, 579, 580, 581];

describe('Economy Of Top Bowlers', function () {
    let expected = { name: 'Economy', data: [['TS Mills', 7.5]] };
    let actual = app.bestEconomy(economyDummyData, valid_match_id_for_economy);
    it('Econmy Should Return Economy Of Top Bowlers', function () {
        assert.deepEqual(actual, expected);
    });


    it('Economy Of Top Bowlers Should Return Object', function () {
        assert.isObject(actual);
    })
});


//  Win percentage data

let matchesPlayedDummyData = {
    'Sunrisers Hyderabad': 5,
    'Royal Challengers Bangalore': 6,
    'Mumbai Indians': 7,
    'Rising Pune Supergiant': 5,
    'Gujarat Lions': 4,
    'Kolkata Knight Riders': 6,
    'Kings XI Punjab': 4,
    'Delhi Daredevils': 4
};

let matcheWonDummyData = {
    'Sunrisers Hyderabad': 3,
    'Royal Challengers Bangalore': 2,
    'Mumbai Indians': 4,
    'Rising Pune Supergiant': 2,
    'Gujarat Lions': 2,
    'Kolkata Knight Riders': 3,
    'Kings XI Punjab': 3,
    'Delhi Daredevils': 1,
}

// Win percentage testing

describe('Win Percentage', function () {
    let actual = app.story(matcheWonDummyData, matchesPlayedDummyData);
    let expected = {
        'Sunrisers Hyderabad': 60,
        'Royal Challengers Bangalore': 33.33333333333333,
        'Mumbai Indians': 57.14285714285714,
        'Rising Pune Supergiant': 40,
        'Gujarat Lions': 50,
        'Kolkata Knight Riders': 50,
        'Kings XI Punjab': 75,
        'Delhi Daredevils': 25
    };
    it('Should Return Win Percentage of Each Team For Particluar Year', function () {
        assert.deepEqual(actual, expected);
    });


    it('Win Percentage Should Return Object', function () {
        assert.isObject(actual);
    });
});