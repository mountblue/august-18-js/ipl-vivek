
module.exports = {

    //maatch id of year
    matchId: function (matches, year) {
        let matchIds = [];
        for (var i = 0; i < matches.length; i++) {
            if (matches[i].season == year) {
                matchIds.push(matches[i].id);
            }
        }
        return matchIds;
    },


    // Number of matches per year

    matchesYearly: function (matchesData) {

        let matchesPerYear = {};



        for (i = 0; i < matchesData.length; i++) {
            if (matchesPerYear[matchesData[i].season]) {
                matchesPerYear[matchesData[i].season]++;
            }
            else {
                matchesPerYear[matchesData[i].season] = 1;
            }
        }

        return matchesPerYear;
    },

    // Number of matches won per year by each team

    matchesWonPerYear: function (matchesData) {
        let matchesWonPerYearByEachTeam = {};
        let allSeasons = {};
        for (i = 0; i < matchesData.length; i++) {
            if (!allSeasons[matchesData[i].season]) {
                allSeasons[matchesData[i].season] = 0;
            }
        }
        // console.log(allSeasons);

        for (i = 0; i < matchesData.length; i++) {
            let year = matchesData[i].season;
            let winingTeam = matchesData[i].winner;

            if (winingTeam != 0) {
                if (matchesWonPerYearByEachTeam[winingTeam]) {
                    matchesWonPerYearByEachTeam[winingTeam][year] += 1;
                }
                else {
                    var years = {};
                    years = Object.assign({}, allSeasons);
                    matchesWonPerYearByEachTeam[winingTeam] = years;
                    matchesWonPerYearByEachTeam[winingTeam][year] = 1;
                }
            }
        }
        return matchesWonPerYearByEachTeam;
    },

    // Extra runs per team in year 2016

    extraRuns: function (deliveries, years) {

        let extraRun = {};
        for (i = 0; i < deliveries.length; i++) {
            if (([deliveries[i].match_id] >= years[0]) && ([deliveries[i].match_id] <= years[years.length - 1])) {
                if (extraRun[deliveries[i].bowling_team] != undefined) {
                    extraRun[deliveries[i].bowling_team] += deliveries[i].extra_runs;
                }
                else {
                    extraRun[deliveries[i].bowling_team] = deliveries[i].extra_runs;
                }

            }
        }
        return extraRun;
    },

    // Economy of bowlers in year 2015

    bestEconomy: function (bestEconomyRawData, years) {
        let bestEconomyYear2015 = {};

        for (var i = 0; i < bestEconomyRawData.length; i++) {
            if ((bestEconomyRawData[i].match_id) >= years[0] && (bestEconomyRawData[i].match_id) <= years[years.length - 1]) {
                if (bestEconomyYear2015[bestEconomyRawData[i].bowler]) {
                    bestEconomyYear2015[bestEconomyRawData[i].bowler]['runs_conceded'] += bestEconomyRawData[i].total_runs - bestEconomyRawData[i].extra_runs;
                    bestEconomyYear2015[bestEconomyRawData[i].bowler]['total_balls']++;
                }
                else {
                    bestEconomyYear2015[bestEconomyRawData[i].bowler] = {};
                    bestEconomyYear2015[bestEconomyRawData[i].bowler]['runs_conceded'] = bestEconomyRawData[i].total_runs - bestEconomyRawData[i].extra_runs;
                    bestEconomyYear2015[bestEconomyRawData[i].bowler]['total_balls'] = 1;
                }
            }
        }
        let bowlerNames = Object.keys(bestEconomyYear2015);
        let economy = {};
        for (i = 0; i < bowlerNames.length; i++) {
            economy[bowlerNames[i]] = bestEconomyYear2015[bowlerNames[i]]['runs_conceded'] / (bestEconomyYear2015[bowlerNames[i]]['total_balls'] / 6);
        }
        // Object to array conversion and sorting as per economy
        var result = Object.keys(economy).map(function (bowler) {
            return [bowler, economy[bowler]];
        });
        result.sort(function (a, b) {
            return a[1] - b[1];
        })
        result = result.slice(0, 10);
        //   Array to object
        var economyOutput = {
            name: "Economy",
            data: result,
        };

        return economyOutput;
    },

    // Story as per available data

    totalNumberOfMatchesPlayedByEachTeam: function (matchesData, year) {
        total_match_played = {};
        for (i = 0; i < matchesData.length; i++) {
            if (matchesData[i].season == year) {
                if (total_match_played[matchesData[i].team1]) {
                    total_match_played[matchesData[i].team1]++;
                }
                if (total_match_played[matchesData[i].team2]) {
                    total_match_played[matchesData[i].team2]++;
                }
                else {
                    total_match_played[matchesData[i].team1] = 1;
                    total_match_played[matchesData[i].team2] = 1;
                }

            }
        }
        return total_match_played;
    },

    matchesWonByEachTeamInParticularYear: function (matchesData, year) {
        let matchesWonByEachTeamInParticularYear = {};
        for (var i = 0; i < matchesData.length; i++) {
            if (matchesData[i].season == year) {
                if (matchesWonByEachTeamInParticularYear[matchesData[i].winner]) {
                    matchesWonByEachTeamInParticularYear[matchesData[i].winner]++;
                }
                else {
                    matchesWonByEachTeamInParticularYear[matchesData[i].winner] = 1;
                }
            }
        }
        return matchesWonByEachTeamInParticularYear;
    },
    story: function (matchesWon, matchesPlayed) {
        let winPercentage = {};
        let teams = Object.keys(matchesWon);
        for (var i = 0; i < teams.length; i++) {
            winPercentage[teams[i]] = (matchesWon[teams[i]] / matchesPlayed[teams[i]]) * 100;
        }
        return winPercentage;
    },
}