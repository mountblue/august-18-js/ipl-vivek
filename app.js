var iplFunctions = require('./ipl');
var fs = require('fs');
let matchesData;
let csvToJson = require('convert-csv-to-json');
let path = require('path')

// console.log(path.join(__dirname, '../data/matches.csv'))

matchesData = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv('./rawData/matches.csv');
let file = iplFunctions.matchesYearly(matchesData);


let idsForYear2016 = iplFunctions.matchId(matchesData, 2016);

/* Number of ipl matches per year */


var string1 = JSON.stringify(file);
fs.writeFile('../data/matches-ipl.json', string1, 'utf8', function (err) {
    console.log("File created");
});


/* Number of matches won by teams per year */


var matchesWon = iplFunctions.matchesWonPerYear(matchesData);

var matchesWonJson = JSON.stringify(matchesWon);
fs.writeFile('../data/matches_won_per_year.json', matchesWonJson, 'utf8', function (err) {
    console.log("File created");
});

/* Extra runns per year by teams */

var extraRunsRawData = csvToJson.fieldDelimiter(',').formatValueByType().getJsonFromCsv('./rawData/deliveries.csv');
var extraRuns = iplFunctions.extraRuns(extraRunsRawData, idsForYear2016);

var extraRunsJsonObject = JSON.stringify(extraRuns);

fs.writeFile('../data/extra_runs_ipl.json', extraRunsJsonObject, 'utf8', function (err) {
    console.log("File created");
});


/* Best Economy bowler */
let idsForYear2015 = iplFunctions.matchId(matchesData, 2015);

var bestEconomy = iplFunctions.bestEconomy(extraRunsRawData, idsForYear2015);

var economyJsonObject = JSON.stringify(bestEconomy);

fs.writeFile('../data/economy_2015_ipl.json', economyJsonObject, 'utf8', function (err) {
    console.log("File created");
});


/* Story */

let totalNumberOfMatchesPlayedByEachTeam = iplFunctions.totalNumberOfMatchesPlayedByEachTeam(matchesData, 2017);
let matchesWonByTeamsInParticularYear = iplFunctions.matchesWonByEachTeamInParticularYear(matchesData, 2017);

let winningPercentageOfEachTeamForParticularYear = iplFunctions.story(matchesWonByTeamsInParticularYear, totalNumberOfMatchesPlayedByEachTeam);

var winPercentageJsonObject = JSON.stringify(winningPercentageOfEachTeamForParticularYear);
fs.writeFile('../data/win_percentage.json', winPercentageJsonObject, 'utf8', function (err) {
    console.log("File created");
});