var extraRunsSeriesData = [];
var extraRunsCategoriesData = [];
$('#extra_runs').click(function () {
    $.ajax({
        url: "../data/extra_runs_ipl.json",
        method: "GET",
    }).done(function (inputData) {
        extraRunsCategoriesData = Object.keys(inputData);
        extraRunsSeriesData = [{ "name": "IPL", "data": Object.values(inputData) }];
        extraRUnsCharts(extraRunsCategoriesData, extraRunsSeriesData);
    });


    function extraRUnsCharts(extraRunsCategoriesData, extraRunsSeriesData) {

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Extra Runs By Teams'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: extraRunsCategoriesData,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Extra Runs'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} Runs</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: extraRunsSeriesData,
        });
    }
})