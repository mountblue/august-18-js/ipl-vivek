var seriesDataForWinPercentage = [];
var inputToHighcharts = [];
$('#winPercentage').click(function () {
    $.ajax({
        url: "../data/win_percentage.json",
        method: "GET",
    }).done(function (data) {
        for (let key in data) {
            seriesDataForWinPercentage.push([key, data[key]]);
        }
        inputToHighcharts = [{ name: "Win Percentage", data: seriesDataForWinPercentage }];
        console.log(inputToHighcharts);
        winPercentageChart(inputToHighcharts)
    });
    function winPercentageChart(inputToHighcharts) {
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Win Percentage Of Each Team'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">IPL Dataset</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Win Percentage'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Economy: <b>{point.y:.1f}</b>'
            },
            series: inputToHighcharts,

            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        });
    }
})