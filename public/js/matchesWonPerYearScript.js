var seriesDataForMatchesWonPerYear = [];
var key = [];
var years;
$('#matchesWonPerYear').click(function () {
    $.ajax({
        url: "../data/matches_won_per_year.json",
        method: "GET",
    }).done(function (data) {
        key = Object.keys(data);
        console.log(key);
        for (var i = 0; i < key.length; i++) {

            seriesDataForMatchesWonPerYear.push({ name: key[i], data: Object.values(data[key[i]]) });
        }
        years = Object.keys(data[key[0]]);
        console.log(years);
        matchesWonChart(years, seriesDataForMatchesWonPerYear);
    });


    function matchesWonChart(years, seriesDataForMatchesWonPerYear) {




        Highcharts.chart('container', {
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Matches Won Per Year Stacked Bar Chart'
            },
            xAxis: {
                categories: years,
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches Per Year'
                }
            },
            legend: {
                reversed: true
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            series: seriesDataForMatchesWonPerYear,
        });

    }

});

