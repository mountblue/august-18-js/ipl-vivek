var seriesDataForNumberOfMatchesPerYear = [];
var key = [];
$(document).ready(function () {
    $.ajax({
        url: "../data/matches-ipl.json",
        method: "GET",
    }).done(function (data) {
        key = Object.keys(data);
        seriesDataForNumberOfMatchesPerYear.push({ "name": "IPL", "data": Object.values(data) });
        renderChart(key, seriesDataForNumberOfMatchesPerYear);
        $('#numberOfMatchesPerYear').click(function () {
            renderChart(key, seriesDataForNumberOfMatchesPerYear);
        });
    });


    function renderChart(keys, seriesDataForNumberOfMatchesPerYear) {
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'IPL Number Of Matches Per Season'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: key,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Matches per year'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: seriesDataForNumberOfMatchesPerYear,
        });

    }
});