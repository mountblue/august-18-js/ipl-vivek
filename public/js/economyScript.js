var seriesDataForEconomy = [];
var key = [];
$('#economy').click(function () {
    $.ajax({
        url: "./data/economy_2015_ipl.json",
        method: "GET",
    }).done(function (data) {
        console.log(data);
        seriesDataForEconomy = [data];
        // renderChart(key,seriesDataForEconomy);
        economyChart(seriesDataForEconomy)
    });
    function economyChart(seriesDataForEconomy) {
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Bowlers Economy'
            },
            subtitle: {
                text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">IPL Dataset</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Economy: <b>{point.y:.1f}</b>'
            },
            series: seriesDataForEconomy,

            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        });
    }
})